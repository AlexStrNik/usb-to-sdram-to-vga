import sys
from PIL import Image

image_path = sys.argv[1]
image = Image.open(image_path)
image = image.resize((640, 480), Image.ANTIALIAS)

output = open(sys.argv[2], 'wb')

for y in range(image.height):
    for x in range(image.width):
        pixel = image.getpixel((x,y))
        b = "{:04b}".format(pixel[0] >> 4)
        g = "{:04b}".format(pixel[1] >> 4)
        r = "{:04b}".format(pixel[2] >> 4)
        pixel = r+g+b+"1111"

        pixel = bytearray(int(pixel[z:z+8], 2) for z in range(0, len(pixel), 8))
        output.write(pixel)

output.close()
