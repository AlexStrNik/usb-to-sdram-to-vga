import socket
import sys

def chunks(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))

CHUNK_SIZE = 2 ^ 18

images_count = len(sys.argv) - 2
bytes_parts = [images_count.to_bytes(1, byteorder='big'), b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00']

for path in sys.argv[1:]:
    img_bytes = open(path, 'rb').read()
    bytes_parts.append(img_bytes)

all_bytes = b"".join(bytes_parts)
all_chunks = chunks(all_bytes, CHUNK_SIZE)

sock = socket.socket()
sock.connect(("127.0.0.1", 2540))

sock.send(b"jtag_open 0\n")

start = 0
for chunk in all_chunks:
    msg = f"jtag_write {start} " + " ".join([str(b) for b in chunk]) + "\n"
    sock.send(msg.encode())
    start += CHUNK_SIZE

sock.send(b"jtag_close\n")
sock.close()