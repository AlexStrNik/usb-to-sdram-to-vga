
module system (
	bridge_address,
	bridge_byte_enable,
	bridge_read,
	bridge_write,
	bridge_write_data,
	bridge_acknowledge,
	bridge_read_data,
	clk_clk,
	reset_reset_n,
	sdram_addr,
	sdram_ba,
	sdram_cas_n,
	sdram_cke,
	sdram_cs_n,
	sdram_dq,
	sdram_dqm,
	sdram_ras_n,
	sdram_we_n,
	sdram_shifted_clk_clk,
	system_clk,
	vga_clk,
	bridge2_address,
	bridge2_byte_enable,
	bridge2_read,
	bridge2_write,
	bridge2_write_data,
	bridge2_acknowledge,
	bridge2_read_data);	

	input	[25:0]	bridge_address;
	input	[15:0]	bridge_byte_enable;
	input		bridge_read;
	input		bridge_write;
	input	[127:0]	bridge_write_data;
	output		bridge_acknowledge;
	output	[127:0]	bridge_read_data;
	input		clk_clk;
	input		reset_reset_n;
	output	[12:0]	sdram_addr;
	output	[1:0]	sdram_ba;
	output		sdram_cas_n;
	output		sdram_cke;
	output		sdram_cs_n;
	inout	[15:0]	sdram_dq;
	output	[1:0]	sdram_dqm;
	output		sdram_ras_n;
	output		sdram_we_n;
	output		sdram_shifted_clk_clk;
	output		system_clk;
	output		vga_clk;
	input	[25:0]	bridge2_address;
	input		bridge2_byte_enable;
	input		bridge2_read;
	input		bridge2_write;
	input	[7:0]	bridge2_write_data;
	output		bridge2_acknowledge;
	output	[7:0]	bridge2_read_data;
endmodule
