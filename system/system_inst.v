	system u0 (
		.bridge_address        (<connected-to-bridge_address>),        //            bridge.address
		.bridge_byte_enable    (<connected-to-bridge_byte_enable>),    //                  .byte_enable
		.bridge_read           (<connected-to-bridge_read>),           //                  .read
		.bridge_write          (<connected-to-bridge_write>),          //                  .write
		.bridge_write_data     (<connected-to-bridge_write_data>),     //                  .write_data
		.bridge_acknowledge    (<connected-to-bridge_acknowledge>),    //                  .acknowledge
		.bridge_read_data      (<connected-to-bridge_read_data>),      //                  .read_data
		.clk_clk               (<connected-to-clk_clk>),               //               clk.clk
		.reset_reset_n         (<connected-to-reset_reset_n>),         //             reset.reset_n
		.sdram_addr            (<connected-to-sdram_addr>),            //             sdram.addr
		.sdram_ba              (<connected-to-sdram_ba>),              //                  .ba
		.sdram_cas_n           (<connected-to-sdram_cas_n>),           //                  .cas_n
		.sdram_cke             (<connected-to-sdram_cke>),             //                  .cke
		.sdram_cs_n            (<connected-to-sdram_cs_n>),            //                  .cs_n
		.sdram_dq              (<connected-to-sdram_dq>),              //                  .dq
		.sdram_dqm             (<connected-to-sdram_dqm>),             //                  .dqm
		.sdram_ras_n           (<connected-to-sdram_ras_n>),           //                  .ras_n
		.sdram_we_n            (<connected-to-sdram_we_n>),            //                  .we_n
		.sdram_shifted_clk_clk (<connected-to-sdram_shifted_clk_clk>), // sdram_shifted_clk.clk
		.system_clk            (<connected-to-system_clk>),            //            system.clk
		.vga_clk               (<connected-to-vga_clk>),               //               vga.clk
		.bridge2_address       (<connected-to-bridge2_address>),       //           bridge2.address
		.bridge2_byte_enable   (<connected-to-bridge2_byte_enable>),   //                  .byte_enable
		.bridge2_read          (<connected-to-bridge2_read>),          //                  .read
		.bridge2_write         (<connected-to-bridge2_write>),         //                  .write
		.bridge2_write_data    (<connected-to-bridge2_write_data>),    //                  .write_data
		.bridge2_acknowledge   (<connected-to-bridge2_acknowledge>),   //                  .acknowledge
		.bridge2_read_data     (<connected-to-bridge2_read_data>)      //                  .read_data
	);

