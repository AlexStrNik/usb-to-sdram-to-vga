module img3vga(clk_50m,
               vga_hs,
               vga_vs,
               vga_r,
               vga_g,
               vga_b,
               sdram_data,
               sdram_addr,
               sdram_clk,
               sdram_ras,
               sdram_cas,
               sdram_cle,
               sdram_ba,
               sdram_cs,
               sdram_we,
               sdram_dqm,
               filter,
               prev,
               next);
    
    input clk_50m;
    input [1:0]filter;

    input prev;
    input next;

    wire rst;
    assign rst = prev || next;
    
    output vga_hs;
    output vga_vs;
    output [3:0]vga_r;
    output [3:0]vga_g;
    output [3:0]vga_b;

    inout [15:0]sdram_data;
    output [12:0]sdram_addr;
    output [1:0]sdram_ba;
    output sdram_cs;
    output sdram_we;
    output sdram_cle;
    output sdram_ras;
    output sdram_cas;
    output sdram_clk;
    output [1:0]sdram_dqm;
    
    wire system_clk;
    wire vga_clk;    
    
    reg [4:0]sub_pixel_count      = 0;
    reg [4:0]sub_pixel_count_last = 0;

    reg [7:0]image_offset = 0;
    reg [7:0]image_offset_next = 0;
    reg [7:0]image_max_offset;
    
    wire end_line;
    wire end_frame;
    
    wire [25:0]bridge_addr;
    wire [15:0]bridge_be;
    wire bridge_read;
    wire bridge_write;
    wire [127:0]bridge_write_data;
    wire [127:0]bridge_read_data;
    wire bridge_ack;

    wire bridge2_addr;
    wire [15:0]bridge2_be;
    reg bridge2_read;
    reg bridge2_read_next;
    wire bridge2_write;
    wire [7:0]bridge2_write_data;
    wire [7:0]bridge2_read_data;
    wire bridge2_ack;

    assign bridge2_addr = 0;
    assign bridge2_write = 0;
    assign bridge2_write_data = 0;
    assign bridge2_be = 15'b111111111111111;

    reg next_btn_last;
    reg prev_btn_last;

    system u0 (
    .bridge_address(bridge_addr),
    .bridge_byte_enable(bridge_be),
    .bridge_read(bridge_read),
    .bridge_write(bridge_write),
    .bridge_write_data(bridge_write_data),
    .bridge_acknowledge(bridge_ack),
    .bridge_read_data(bridge_read_data),
    .bridge2_address(bridge2_addr),
    .bridge2_byte_enable(bridge2_be),
    .bridge2_read(bridge2_read),
    .bridge2_write(bridge2_write),
    .bridge2_write_data(bridge2_write_data),
    .bridge2_acknowledge(bridge2_ack),
    .bridge2_read_data(bridge2_read_data),
    .clk_clk(clk_50m),
    .system_clk(system_clk),
    .reset_reset_n(rst),
    .sdram_addr(sdram_addr),
    .sdram_ba(sdram_ba),
    .sdram_cas_n(sdram_cas),
    .sdram_cke(sdram_cle),
    .sdram_cs_n(sdram_cs),
    .sdram_dq(sdram_data),
    .sdram_dqm(sdram_dqm),
    .sdram_ras_n(sdram_ras),
    .sdram_we_n(sdram_we),
    .sdram_shifted_clk_clk(sdram_clk),
    .vga_clk(vga_clk)
    );
    
    vga_timer u1 (
    .clk(vga_clk),
    .reset_n(rst),
    .h_sync(h_sync),
    .v_sync(v_sync),
    .disp_ena(disp_ena),
    .col(col),
    .row(row),
    .end_line(end_line),
    .end_frame(end_frame)
    );

    reg [6:0]buffer_read_addr;
    wire [127:0]buffer_read_data;
    wire [25:0]base_addr;

    buffer u2 (
    .read_clock(vga_clk),
    .rst(rst),
    .interface_addr(bridge_addr),
    .interface_be(bridge_be),
    .interface_read(bridge_read),
    .interface_write(bridge_write),
    .interface_read_data(bridge_read_data),
    .interface_write_data(bridge_write_data),
    .interface_ack(bridge_ack),
    .interface_clock(system_clk),
    .read_addr(buffer_read_addr),
    .read_data(buffer_read_data),
    .start(end_line),
    .base_address(base_addr),
    );
    
    wire h_sync;
    wire v_sync;
    wire disp_ena;
    wire [9:0]col;
    wire [9:0]row;
    wire [3:0]red;
    wire [3:0]green;
    wire [3:0]blue;
    wire [3:0]red_f;
    wire [3:0]green_f;
    wire [3:0]blue_f;
    reg h_sync_delay;
    reg v_sync_delay;
    reg disp_ena_delay;

    wire [15:0]pixel;

    assign pixel = buffer_read_data[(16 * sub_pixel_count_last) +: 16];
    
    assign red   = pixel[15:12];
    assign green = pixel[3:0];
    assign blue  = pixel[7:4];
    
    assign red_f   = (filter == 2'b01)? (red + green + blue) / 3: ((filter == 2'b10)? 15 - red: red);
    assign green_f = (filter == 2'b01)? (red + green + blue) / 3: ((filter == 2'b10)? 15 - green: green);
    assign blue_f  = (filter == 2'b01)? (red + green + blue) / 3: ((filter == 2'b10)? 15 - blue: blue);
    
    wire [9:0]next_row;
    assign next_row = (row == 479)? 0 : row + 1'b1;
    
    assign base_addr = (614400 * image_offset) + (1280 * next_row) + 16;

    always @(posedge system_clk)
    begin
        if (!rst)
        begin
            next_btn_last <= 0;
            prev_btn_last <= 0;
            image_offset <= 0;
            bridge2_read <= 1;
        end
        else
        begin
            next_btn_last <= next;
            prev_btn_last <= prev;
            image_offset <= image_offset_next;
            bridge2_read <= bridge2_read_next;
        end
    end

    reg max_we = 0;

    always @(posedge system_clk)
    begin
        if (max_we)
        begin
            image_max_offset <= bridge2_read_data;
        end
    end

    wire next_btn_rising_edge;    
    wire prev_btn_rising_edge;

    assign next_btn_rising_edge = (next == 1'b0) && (next_btn_last == 1'b1);
    assign prev_btn_rising_edge = (prev == 1'b0) && (prev_btn_last == 1'b1);

    always @(next_btn_rising_edge, prev_btn_rising_edge, rst, bridge2_ack)
    begin
        image_offset_next = image_offset;
        bridge2_read_next = bridge2_read;
        max_we = 0;

        if (next_btn_rising_edge)
        begin
            if (image_offset < image_max_offset)
            begin
                image_offset_next = image_offset + 1'b1;
            end
            else
            begin
                image_offset_next = 0;
            end
        end
        else if (prev_btn_rising_edge)
        begin
            if (image_offset > 0)
            begin
                image_offset_next = image_offset - 1'b1;
            end
            else
            begin
                image_offset_next = image_max_offset;
            end
        end

        if (bridge2_ack)
        begin
            bridge2_read_next = 0;
            max_we = 1;
        end
    end

    always @(posedge vga_clk)
    begin
        if (end_line)
        begin
            buffer_read_addr <= 0;
            sub_pixel_count  <= 0;
        end
        else if (disp_ena)
        begin
            if (sub_pixel_count == 7)
            begin
                sub_pixel_count  <= 0;
                buffer_read_addr <= buffer_read_addr + 1;
            end
            else
            begin
                sub_pixel_count <= sub_pixel_count + 1;
            end
        end
            
        sub_pixel_count_last <= sub_pixel_count;
    end

    always @(posedge vga_clk) 
    begin
        h_sync_delay <= h_sync;
        v_sync_delay <= v_sync;
        disp_ena_delay <= disp_ena;
    end

    assign vga_hs = h_sync_delay;
    assign vga_vs = v_sync_delay;

    assign vga_r = disp_ena_delay? red_f   : 4'b0000;
    assign vga_g = disp_ena_delay? green_f : 4'b0000;
    assign vga_b = disp_ena_delay? blue_f  : 4'b0000;
endmodule
