# USB to SDRAM to VGA

Project allowing displaying of images through the VGA connector on the DE10-Lite board by storing the image in SDRAM.

## How it works

QSYS is used to create the hardest parts of the project, like the SDRAM controller, PLL,
JTAG controller and interconnections. Uploading to SDRAM is pretty easy, it works via the `JTAG to Avalon-MM` component.
So nothing interesting here. The displaying is a more complicated thing.
First, we need an `External Bus to Avalon Bridge` with 128-bit width.
Then we need to write a `Line Buffer`, that will load the whole line (640 pixels) to memory while
VGA down-time. 
(In VGA timing specification we have some time at the start of the line when we aren't writing pixels to the screen). After buffering, we are displaying pixels on the screen. Each pixel encoded in two bytes (4-bit for each RGB component, and some zero-filled space for alignment). Thus, our buffer is an array of 128-bit width and length of 80. So while iterating buffer, we also need iterate pixels in the current element of the buffer.

To converting images and uploading them to FPGA, I have written some python scripts, that can be found in the `scripts` directory. Also, there is a TCL script, taken from [here](https://github.com/hildebrandmw/de10lite-hdl/blob/master/components/usb-blaster/jtag_server.tcl) and slightly rewritten (I removed the functionality to write log to not spam in the SystemConsole)

## Links

Most part of the code was taken from this [project](https://github.com/hildebrandmw/de10lite-hdl/tree/master/projects/play_gif) and then adapted and translate to Verilog. In the future, I may rewrite some parts to improve performance, but not for now. These links also were very helpful:

- https://www.intel.com/content/dam/altera-www/global/en_US/portal/dsn/42/doc-us-dsnbk-42-2912030810549-de10-lite-user-manual.pdf
- https://www.chipverify.com/verilog/verilog-arrays-memories
- https://people.ece.cornell.edu/land/courses/ece5760/DE1_SOC/External_Bus_to_Avalon_Bridge.pdf
