module vga_timer(clk,
                 reset_n,
                 h_sync,
                 v_sync,
                 disp_ena,
                 col,
                 row,
                 end_line,
                 end_frame);
    
    input clk;
    input reset_n;
    output reg h_sync;
    output reg v_sync;
    output reg disp_ena;
    output reg [9:0] col;
    output reg [9:0] row;
    output reg end_line;
    output reg end_frame;
    
    localparam H_PIXELS = 640;
    localparam H_FP     = 16;
    localparam H_PULSE  = 96;
    localparam H_BP     = 48;
    localparam H_POL    = 1'b0;
    localparam V_PIXELS = 480;
    localparam V_FP     = 10;
    localparam V_PULSE  = 2;
    localparam V_BP     = 33;
    localparam V_POL    = 1'b0;
    
    localparam h_period = H_PULSE + H_BP + H_PIXELS + H_FP;
    localparam v_period = V_PULSE + V_BP + V_PIXELS + V_FP;
    
    
    reg [10:0] h_count;
    reg [10:0] v_count;
    
    always @(posedge clk)
    begin
    
    if (reset_n == 1'b0)
    begin
        h_count  <= 0;
        v_count  <= 0;
        h_sync   <= ~H_POL;
        v_sync   <= ~V_POL;
        disp_ena <= 1'b0;
        col      <= 0;
        row      <= 0;
    end
    else
    begin
        if (h_count < h_period - 1)
        begin
            h_count <= h_count + 1;
        end
        else
        begin
            h_count <= 0;
            if (v_count < v_period - 1)
            begin
                v_count <= v_count + 1;
            end
            else
            begin
                v_count <= 0;
            end
        end
        
        
        if ((h_count < H_PIXELS + H_FP) || (h_count > H_PIXELS + H_FP + H_PULSE))
        begin
            h_sync <= ~H_POL;
        end
        else
        begin
            h_sync <= H_POL;
        end
        
        
        if ((v_count < V_PIXELS + V_FP) || (v_count > V_PIXELS + V_FP + V_PULSE))
        begin
            v_sync <= ~V_POL;
        end
        else
        begin
            v_sync <= V_POL;
        end
        
        
        if (h_count < H_PIXELS)
        begin
            col <= h_count;
        end
        
        if (v_count < V_PIXELS)
        begin
            row <= v_count;
        end
        
        
        if (h_count < H_PIXELS && v_count < V_PIXELS)
        begin
            disp_ena <= 1'b1;
        end
        else
        begin
            disp_ena <= 1'b0;
        end
        
        if (h_count == H_PIXELS)
        begin
            end_line <= 1'b1;
        end
        else
        begin
            end_line <= 1'b0;
        end
        
        if ((h_count == H_PIXELS - 1) && (v_count == V_PIXELS - 1)) begin
            end_frame <= 1'b1;
        end
        else
        begin
            end_frame <= 1'b0;
        end
    end
end
    
endmodule
