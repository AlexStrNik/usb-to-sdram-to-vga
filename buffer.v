module buffer (interface_clock,
               read_clock,
               rst,
               interface_addr,
               interface_be,
               interface_read,
               interface_read_data,
               interface_write,
               interface_write_data,
               interface_ack,
               read_addr,
               read_data,
               start,
               base_address,
               );
    
    input interface_clock;
    input read_clock;
    input rst;
    output [25:0]interface_addr;
    output [15:0]interface_be;
    output reg interface_read;
    input [127:0]interface_read_data;
    output [127:0]interface_write_data;
    output interface_write;
    input interface_ack;
    input [6:0]read_addr;
    output [127:0]read_data;
    input start;
    input [25:0]base_address;
    
    reg [127:0]buffer_read_data;
    reg start_last;
    wire start_rising_edge;
    reg [6:0]buffer_write_addr      = 0;
    reg [6:0]buffer_write_addr_next = 0;
    reg [25:0]buffer_base_addr      = 0;
    reg [25:0]buffer_base_addr_next = 0;
    reg buffer_we                   = 1'b0;
    reg [127:0]buffer[79:0];
    wire [6:0]buffer_read_addr;
    wire [127:0]buffer_write_data;    
    
    localparam IDLE      = 1'b0;
    localparam BUFFERING = 1'b1;
    
    reg buffer_state      = IDLE;
    reg buffer_state_next = IDLE;
    
    always @(posedge read_clock)
    begin
        buffer_read_data <= buffer[buffer_read_addr];
    end
    
    always @(posedge interface_clock)
    begin
        start_last = start;
    end
    
    assign start_rising_edge = (start_last == 1'b0) && (start == 1'b1);
    
    always @(posedge interface_clock)
    begin
        if (!rst)
        begin
            buffer_state      <= IDLE;
            buffer_base_addr  <= 0;
            buffer_write_addr <= 0;
        end
        else
        begin
            buffer_state      <= buffer_state_next;
            buffer_base_addr  <= buffer_base_addr_next;
            buffer_write_addr <= buffer_write_addr_next;
        end
    end
    
    always @(posedge interface_clock)
    begin
        if (buffer_we)
        begin
            buffer[buffer_write_addr] <= buffer_write_data;
        end
    end
    
    assign interface_addr       = buffer_base_addr + (buffer_write_addr * 16);
    assign buffer_write_data    = interface_read_data;
    assign interface_write      = 0;
    assign interface_write_data = 0;
    assign interface_be         = 15'b111111111111111;
    assign read_data = buffer_read_data;
    assign buffer_read_addr = read_addr;
    
    always @(*)
    begin
        buffer_state_next      = buffer_state;
        buffer_write_addr_next = buffer_write_addr;
        buffer_base_addr_next  = buffer_base_addr;
        
        interface_read = 1'b0;
        buffer_we  = 1'b0;
        
        case (buffer_state)
            IDLE:
            begin
                if (start_rising_edge)
                begin
                    buffer_write_addr_next = 1'b0;
                    buffer_base_addr_next  = base_address;
                    buffer_state_next      = BUFFERING;
                end
            end
            BUFFERING:
            begin
                interface_read = 1'b1;
                
                if (interface_ack)
                begin
                    buffer_write_addr_next = buffer_write_addr + 1'b1;
                    buffer_we              = 1'b1;
                    
                    if (buffer_write_addr == 79)
                    begin
                        buffer_state_next = IDLE;
                    end
                end
            end
        endcase
    end
endmodule
